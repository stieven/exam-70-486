﻿namespace TestWebApp.Models
{
    public class BankAccount
    {
        private static int _ids = 1;

        private readonly string _owner;
        private readonly int _id;

        public BankAccount(string owner)
            : this(owner, 0)
        {
        }

        public BankAccount(string owner, decimal initialBalance)
        {
            _id = _ids++;

            Balance = initialBalance;
            _owner = owner;
        }

        public decimal Balance { get; set; }

        public int Id => _id;

        public string Owner => _owner;
    }
}