﻿using System.Web.Mvc;
using TestWebApp.Services;

namespace TestWebApp.Controllers
{
    public class BankAccountController : Controller
    {
        private readonly BankAccountService _bankAccountService;

        public BankAccountController()
        {
            _bankAccountService = new BankAccountService();
        }

        // GET: BankAccount
        public ActionResult Index()
        {
            var accounts = _bankAccountService.GetAccounts();
            return View(accounts);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string owner)
        {
            _bankAccountService.CreateAccount(owner);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var account = _bankAccountService.GetAccount(id);
            return View(account);
        }

        public ActionResult Details(int id)
        {
            var account = _bankAccountService.GetAccount(id);
            return View(account);
        }

        [HttpPost]
        public void Debit(int id, decimal amount)
        {
            _bankAccountService.Debit(id, amount);
        }

        [HttpPost]
        public void Deposit(int id, decimal amount)
        {
            _bankAccountService.Deposit(id, amount);
        }
    }
}