﻿using System.Collections.Generic;
using System.Linq;
using TestWebApp.Models;

namespace TestWebApp.Repositories
{
    public class BankAccountRepository //: IBankAccountRepository
    {
        private static readonly List<BankAccount> Data = new List<BankAccount>()
        {
            //new BankAccount("Stieven", 0)
        };

        public void Create(string owner)
        {
            Data.Add(new BankAccount(owner));
        }

        public BankAccount Get(int id)
        {
            return Data.SingleOrDefault(ba => ba.Id == id);
        }

        public IEnumerable<BankAccount> GetAll()
        {
            return Data;
        }
    }
}