﻿using System.Collections.Generic;
using TestWebApp.Models;
using TestWebApp.Repositories;

namespace TestWebApp.Services
{
    public class BankAccountService
    {
        private readonly BankAccountRepository _repository;

        public BankAccountService()
        {
            _repository = new BankAccountRepository();
        }

        public void CreateAccount(string owner)
        {
            _repository.Create(owner);
        }

        public BankAccount GetAccount(int id)
        {
            return _repository.Get(id);
        }

        public IEnumerable<BankAccount> GetAccounts()
        {
            return _repository.GetAll();
        }

        public void Deposit(int bankAccountId, decimal amount)
        {
            var account = _repository.Get(bankAccountId);
            account.Balance += amount;
        }

        public void Debit(int bankAccountId, decimal amount)
        {
            var account = _repository.Get(bankAccountId);

            account.Balance -= amount;
        }
    }
}