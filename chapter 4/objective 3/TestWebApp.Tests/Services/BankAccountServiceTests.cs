﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestWebApp.Services;

namespace TestWebApp.Tests.Services
{
    [TestClass]
    public class BankAccountServiceTests
    {
        [TestClass]
        public class Deposit
        {
            [TestMethod, ExpectedException(typeof(NullReferenceException))]
            public void Without_Valid_Account_Should_Throw_NullReferenceException()
            {
                // arrange
                var service = new BankAccountService();

                // act
                service.Deposit(1, 5);

                // assert => see expectedexception attribute
            }

            //[TestMethod]
            //public void Should_Add_Amount_To_Balance()
            //{
            //    // arrange
            //    var service = new BankAccountService();
            //    service.CreateAccount("UnitTest");
            //    var bankAccount = service.GetAccount(1);
            //    var balance = bankAccount.Balance;
            //    var amount = 5;

            //    // act
            //    service.Deposit(bankAccount.Id, amount);
            //    bankAccount = service.GetAccount(bankAccount.Id);

            //    // assert
            //    Assert.IsNotNull(bankAccount);
            //    Assert.AreEqual(balance + amount, bankAccount.Balance);
            //}

            //[TestMethod, ExpectedException(typeof(NullReferenceException))]
            //public void Without_Valid_Account_Should_Throw_NullReferenceException_V2()
            //{
            //    // arrange
            //    var repositoryMock = new Mock<IBankAccountRepository>();
            //    repositoryMock
            //        .Setup(m => m.Get(It.IsAny<int>()))
            //        .Returns(() => null);
            //    var service = new BankAccountService(repositoryMock.Object);

            //    // act
            //    service.Deposit(1, 5);

            //    // assert => see expectedexception attribute
            //}
        }

        [TestClass]
        public class Debit
        {
            //[TestMethod]
            //public void Should_Subtract_Amount_From_Balance()
            //{
            //    // arrange
            //    var service = new BankAccountService();
            //    service.CreateAccount("UnitTest");
            //    var bankAccount = service.GetAccount(1);
            //    var balance = bankAccount.Balance += 10;
            //    var amount = 5;

            //    // act
            //    service.Debit(bankAccount.Id, amount);
            //    bankAccount = service.GetAccount(bankAccount.Id);

            //    // assert
            //    Assert.IsNotNull(bankAccount);
            //    Assert.AreEqual(balance - amount, bankAccount.Balance);
            //}

            //[TestMethod]
            //public void Should_Subtract_Amount_From_Balance_V2()
            //{
            //    //arrange
            //    var repository = new Mock<IBankAccountRepository>();
            //    var initialBalance = 15;
            //    var amount = 5;
            //    var bankAccount = new BankAccount("unittest", initialBalance);

            //    repository
            //        .Setup(m => m.Get(It.Is((int id) => bankAccount.Id == id)))
            //        .Returns(() => bankAccount);

            //    var service = new BankAccountService(repository.Object);

            //    // act
            //    service.Debit(bankAccount.Id, amount);

            //    // assert
            //    Assert.IsNotNull(bankAccount);
            //    Assert.AreEqual(initialBalance - amount, bankAccount.Balance);
            //}

            [TestMethod, ExpectedException(typeof(NullReferenceException))]
            public void Without_Valid_Account_Should_Throw_NullReferenceException()
            {
                // arrange
                var service = new BankAccountService();

                // act
                service.Debit(1, 5);

                // assert => see expectedexception attribute
            }

            //[TestMethod, ExpectedException(typeof(NullReferenceException))]
            //public void Without_Valid_Account_Should_Throw_NullReferenceException_V2()
            //{
            //    // arrange
            //    var repositoryMock = new Mock<IBankAccountRepository>();
            //    repositoryMock
            //        .Setup(m => m.Get(It.IsAny<int>()))
            //        .Returns(() => null);
            //    var service = new BankAccountService(repositoryMock.Object);

            //    // act
            //    service.Debit(1, 5);

            //    // assert => see expectedexception attribute
            //}

            //[TestMethod]
            //public void With_Amount_Larger_Than_Balance_Should_Fail()
            //{
            //    // arrange
            //    var service = new BankAccountService();
            //    service.CreateAccount("UnitTest");
            //    var bankAccount = service.GetAccount(1);
            //    var balance = bankAccount.Balance;
            //    var amount = 5;

            //    // act
            //    service.Debit(bankAccount.Id, amount);
            //    bankAccount = service.GetAccount(bankAccount.Id);

            //    // assert
            //    Assert.IsNotNull(bankAccount);
            //    Assert.AreEqual(balance - amount, bankAccount.Balance);
            //}
        }
    }
}