﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class User
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string FirstName { get; set; }
    }
}