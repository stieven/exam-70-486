﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace WebApp.Models
{
    public class User
    {
        [Required, Display(Name = "User Name")]
        [Remote("IsValidUserName", "User")]
        public string UserName { get; set; }

        [Required]
        public string Name { get; set; }

        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }
    }

    public class UserService
    {
        private static readonly Dictionary<string, User> Users = new Dictionary<string, User>();

        static UserService()
        {
            var face = new User() { FirstName = "Michiel", Name = "Staessen", UserName = "Face" };
            var samjayyy = new User() { FirstName = "Sam", Name = "Segers", UserName = "Samjayyy" };
            var murdock = new User() { FirstName = "Stieven", Name = "D'Hooge", UserName = "Murdock" };

            Users.Add(face.UserName.ToUpperInvariant(), face);
            Users.Add(samjayyy.UserName.ToUpperInvariant(), samjayyy);
            Users.Add(murdock.UserName.ToUpperInvariant(), murdock);
        }

        public IEnumerable<User> GetAll()
        {
            return Users.Values;
        }

        public User Get(string userName)
        {
            userName = userName.ToUpperInvariant();
            User user;

            if (Users.TryGetValue(userName, out user))
            {
                return user;
            }

            return null;
        }

        public void Add(User user)
        {
            string userName = user.UserName.ToUpperInvariant();

            Users.Add(userName, user);
        }
    }
}